#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define TILE_TYPE_GAME_OBJECT            0x00000001
#define TILE_TYPE_NON_COLLIDING          0x00000002
#define TILE_TYPE_COLLIDING              0x00000004
#define TILE_TYPE_PUSH_PULL_BLOCK        0x00000008
#define TILE_TYPE_MINIMAP                0x00000010
#define TILE_TYPE_WEAPON                 0x00000020
#define TILE_TYPE_ITEM                   0x00000040
#define TILE_TYPE_CHARACTER              0x00000080

#define TILE_WEAPON_LIGHT_BLASTER        0x00001000
#define TILE_WEAPON_HEAVY_BLASTER        0x00002000
#define TILE_WEAPON_LIGHTSABER           0x00004000
#define TILE_WEAPON_THE_FORCE            0x00008000

#define TILE_ITEM_KEYCARD                0x00001000
#define TILE_ITEM_PUZZLE_ITEM            0x00002000
#define TILE_ITEM_PUZZLE_RARE_ITEM       0x00004000
#define TILE_ITEM_PUZZLE_KEY_ITEM        0x00008000
#define TILE_ITEM_LOCATOR                0x00010000
#define TILE_ITEM_MEDIKIT                0x00020000

#define TILE_DOOR                        0x00001000

#define TILE_MINIMAP_HOME                0x00002000
#define TILE_MINIMAP_PUZZLE_UNSOLVED     0x00004000
#define TILE_MINIMAP_PUZZLE_SOLVED       0x00008000
#define TILE_MINIMAP_GATEWAY_UNSOLVED    0x00010000
#define TILE_MINIMAP_GATEWAY_SOLVED      0x00020000
#define TILE_MINIMAP_UP_WALL_LOCKED      0x00040000
#define TILE_MINIMAP_DOWN_WALL_LOCKED    0x00080000
#define TILE_MINIMAP_LEFT_WALL_LOCKED    0x00100000
#define TILE_MINIMAP_RIGHT_WALL_LOCKED   0x00200000
#define TILE_MINIMAP_UP_WALL_UNLOCKED    0x00400000
#define TILE_MINIMAP_DOWN_WALL_UNLOCKED  0x00800000
#define TILE_MINIMAP_LEFT_WALL_UNLOCKED  0x01000000
#define TILE_MINIMAP_RIGHT_WALL_UNLOCKED 0x02000000
#define TILE_MINIMAP_OBJECTIVE           0x04000000

#define IZON_TRIGGER_LOCATION           0x00
#define IZON_SPAWN_LOCATION             0x01
#define IZON_FORCE_RELATED_LOCATION     0x02
#define IZON_VEHICLE_TO_SEC_MAP         0x03
#define IZON_VEHICLE_TO_PRI_MAP         0x04
#define IZON_OBJECT_LOCATOR             0x05
#define IZON_CRATE_ITEM                 0x06
#define IZON_PUZZLE_NPC                 0x07
#define IZON_CRATE_WEAPON               0x08
#define IZON_DOOR_IN                    0x09
#define IZON_DOOR_OUT                   0x0A
#define IZON_UNUSED                     0x0B
#define IZON_LOCK                       0x0C
#define IZON_TELEPORTER                 0x0D
#define IZON_XWING_FROM_DAGOBAH         0x0E
#define IZON_XWING_TO_DAGOBAH           0x0F

#define ZONE_TYPE_DESERT                0x01
#define ZONE_TYPE_SNOW                  0x02
#define ZONE_TYPE_FOREST                0x03
#define ZONE_TYPE_SWAMP                 0x05

#define ICHA_NUM_PROPERTIES 29

#define show(X,F,M) if ((X & F) == F) { printf(#M " "); }

/**
 * Esta es la paleta del juego en crudo.
 */
static unsigned char palette[] = {
  0xFF, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x8B, 0x00, 0xC3, 0xCF, 0x4B, 0x00,
  0x8B, 0xA3, 0x1B, 0x00, 0x57, 0x77, 0x00, 0x00, 0x8B, 0xA3, 0x1B, 0x00, 0xC3, 0xCF, 0x4B, 0x00,
  0xFB, 0xFB, 0xFB, 0x00, 0xEB, 0xE7, 0xE7, 0x00, 0xDB, 0xD3, 0xD3, 0x00, 0xCB, 0xC3, 0xC3, 0x00,
  0xBB, 0xB3, 0xB3, 0x00, 0xAB, 0xA3, 0xA3, 0x00, 0x9B, 0x8F, 0x8F, 0x00, 0x8B, 0x7F, 0x7F, 0x00,
  0x7B, 0x6F, 0x6F, 0x00, 0x67, 0x5B, 0x5B, 0x00, 0x57, 0x4B, 0x4B, 0x00, 0x47, 0x3B, 0x3B, 0x00,
  0x33, 0x2B, 0x2B, 0x00, 0x23, 0x1B, 0x1B, 0x00, 0x13, 0x0F, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0xC7, 0x43, 0x00, 0x00, 0xB7, 0x43, 0x00, 0x00, 0xAB, 0x3F, 0x00, 0x00, 0x9F, 0x3F, 0x00,
  0x00, 0x93, 0x3F, 0x00, 0x00, 0x87, 0x3B, 0x00, 0x00, 0x7B, 0x37, 0x00, 0x00, 0x6F, 0x33, 0x00,
  0x00, 0x63, 0x33, 0x00, 0x00, 0x53, 0x2B, 0x00, 0x00, 0x47, 0x27, 0x00, 0x00, 0x3B, 0x23, 0x00,
  0x00, 0x2F, 0x1B, 0x00, 0x00, 0x23, 0x13, 0x00, 0x00, 0x17, 0x0F, 0x00, 0x00, 0x0B, 0x07, 0x00,
  0x4B, 0x7B, 0xBB, 0x00, 0x43, 0x73, 0xB3, 0x00, 0x43, 0x6B, 0xAB, 0x00, 0x3B, 0x63, 0xA3, 0x00,
  0x3B, 0x63, 0x9B, 0x00, 0x33, 0x5B, 0x93, 0x00, 0x33, 0x5B, 0x8B, 0x00, 0x2B, 0x53, 0x83, 0x00,
  0x2B, 0x4B, 0x73, 0x00, 0x23, 0x4B, 0x6B, 0x00, 0x23, 0x43, 0x5F, 0x00, 0x1B, 0x3B, 0x53, 0x00,
  0x1B, 0x37, 0x47, 0x00, 0x1B, 0x33, 0x43, 0x00, 0x13, 0x2B, 0x3B, 0x00, 0x0B, 0x23, 0x2B, 0x00,
  0xD7, 0xFF, 0xFF, 0x00, 0xBB, 0xEF, 0xEF, 0x00, 0xA3, 0xDF, 0xDF, 0x00, 0x8B, 0xCF, 0xCF, 0x00,
  0x77, 0xC3, 0xC3, 0x00, 0x63, 0xB3, 0xB3, 0x00, 0x53, 0xA3, 0xA3, 0x00, 0x43, 0x93, 0x93, 0x00,
  0x33, 0x87, 0x87, 0x00, 0x27, 0x77, 0x77, 0x00, 0x1B, 0x67, 0x67, 0x00, 0x13, 0x5B, 0x5B, 0x00,
  0x0B, 0x4B, 0x4B, 0x00, 0x07, 0x3B, 0x3B, 0x00, 0x00, 0x2B, 0x2B, 0x00, 0x00, 0x1F, 0x1F, 0x00,
  0xDB, 0xEB, 0xFB, 0x00, 0xD3, 0xE3, 0xFB, 0x00, 0xC3, 0xDB, 0xFB, 0x00, 0xBB, 0xD3, 0xFB, 0x00,
  0xB3, 0xCB, 0xFB, 0x00, 0xA3, 0xC3, 0xFB, 0x00, 0x9B, 0xBB, 0xFB, 0x00, 0x8F, 0xB7, 0xFB, 0x00,
  0x83, 0xB3, 0xF7, 0x00, 0x73, 0xA7, 0xFB, 0x00, 0x63, 0x9B, 0xFB, 0x00, 0x5B, 0x93, 0xF3, 0x00,
  0x5B, 0x8B, 0xEB, 0x00, 0x53, 0x8B, 0xDB, 0x00, 0x53, 0x83, 0xD3, 0x00, 0x4B, 0x7B, 0xCB, 0x00,
  0x9B, 0xC7, 0xFF, 0x00, 0x8F, 0xB7, 0xF7, 0x00, 0x87, 0xB3, 0xEF, 0x00, 0x7F, 0xA7, 0xF3, 0x00,
  0x73, 0x9F, 0xEF, 0x00, 0x53, 0x83, 0xCF, 0x00, 0x3B, 0x6B, 0xB3, 0x00, 0x2F, 0x5B, 0xA3, 0x00,
  0x23, 0x4F, 0x93, 0x00, 0x1B, 0x43, 0x83, 0x00, 0x13, 0x3B, 0x77, 0x00, 0x0B, 0x2F, 0x67, 0x00,
  0x07, 0x27, 0x57, 0x00, 0x00, 0x1B, 0x47, 0x00, 0x00, 0x13, 0x37, 0x00, 0x00, 0x0F, 0x2B, 0x00,
  0xFB, 0xFB, 0xE7, 0x00, 0xF3, 0xF3, 0xD3, 0x00, 0xEB, 0xE7, 0xC7, 0x00, 0xE3, 0xDF, 0xB7, 0x00,
  0xDB, 0xD7, 0xA7, 0x00, 0xD3, 0xCF, 0x97, 0x00, 0xCB, 0xC7, 0x8B, 0x00, 0xC3, 0xBB, 0x7F, 0x00,
  0xBB, 0xB3, 0x73, 0x00, 0xAF, 0xA7, 0x63, 0x00, 0x9B, 0x93, 0x47, 0x00, 0x87, 0x7B, 0x33, 0x00,
  0x6F, 0x67, 0x1F, 0x00, 0x5B, 0x53, 0x0F, 0x00, 0x47, 0x43, 0x00, 0x00, 0x37, 0x33, 0x00, 0x00,
  0xFF, 0xF7, 0xF7, 0x00, 0xEF, 0xDF, 0xDF, 0x00, 0xDF, 0xC7, 0xC7, 0x00, 0xCF, 0xB3, 0xB3, 0x00,
  0xBF, 0x9F, 0x9F, 0x00, 0xB3, 0x8B, 0x8B, 0x00, 0xA3, 0x7B, 0x7B, 0x00, 0x93, 0x6B, 0x6B, 0x00,
  0x83, 0x57, 0x57, 0x00, 0x73, 0x4B, 0x4B, 0x00, 0x67, 0x3B, 0x3B, 0x00, 0x57, 0x2F, 0x2F, 0x00,
  0x47, 0x27, 0x27, 0x00, 0x37, 0x1B, 0x1B, 0x00, 0x27, 0x13, 0x13, 0x00, 0x1B, 0x0B, 0x0B, 0x00,
  0xF7, 0xB3, 0x37, 0x00, 0xE7, 0x93, 0x07, 0x00, 0xFB, 0x53, 0x0B, 0x00, 0xFB, 0x00, 0x00, 0x00,
  0xCB, 0x00, 0x00, 0x00, 0x9F, 0x00, 0x00, 0x00, 0x6F, 0x00, 0x00, 0x00, 0x43, 0x00, 0x00, 0x00,
  0xBF, 0xBB, 0xFB, 0x00, 0x8F, 0x8B, 0xFB, 0x00, 0x5F, 0x5B, 0xFB, 0x00, 0x93, 0xBB, 0xFF, 0x00,
  0x5F, 0x97, 0xF7, 0x00, 0x3B, 0x7B, 0xEF, 0x00, 0x23, 0x63, 0xC3, 0x00, 0x13, 0x53, 0xB3, 0x00,
  0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 0xEF, 0x00, 0x00, 0x00, 0xE3, 0x00, 0x00, 0x00, 0xD3, 0x00,
  0x00, 0x00, 0xC3, 0x00, 0x00, 0x00, 0xB7, 0x00, 0x00, 0x00, 0xA7, 0x00, 0x00, 0x00, 0x9B, 0x00,
  0x00, 0x00, 0x8B, 0x00, 0x00, 0x00, 0x7F, 0x00, 0x00, 0x00, 0x6F, 0x00, 0x00, 0x00, 0x63, 0x00,
  0x00, 0x00, 0x53, 0x00, 0x00, 0x00, 0x47, 0x00, 0x00, 0x00, 0x37, 0x00, 0x00, 0x00, 0x2B, 0x00,
  0x00, 0xFF, 0xFF, 0x00, 0x00, 0xE3, 0xF7, 0x00, 0x00, 0xCF, 0xF3, 0x00, 0x00, 0xB7, 0xEF, 0x00,
  0x00, 0xA3, 0xEB, 0x00, 0x00, 0x8B, 0xE7, 0x00, 0x00, 0x77, 0xDF, 0x00, 0x00, 0x63, 0xDB, 0x00,
  0x00, 0x4F, 0xD7, 0x00, 0x00, 0x3F, 0xD3, 0x00, 0x00, 0x2F, 0xCF, 0x00, 0x97, 0xFF, 0xFF, 0x00,
  0x83, 0xDF, 0xEF, 0x00, 0x73, 0xC3, 0xDF, 0x00, 0x5F, 0xA7, 0xCF, 0x00, 0x53, 0x8B, 0xC3, 0x00,
  0x2B, 0x2B, 0x00, 0x00, 0x23, 0x23, 0x00, 0x00, 0x1B, 0x1B, 0x00, 0x00, 0x13, 0x13, 0x00, 0x00,
  0xFF, 0x0B, 0x00, 0x00, 0xFF, 0x00, 0x4B, 0x00, 0xFF, 0x00, 0xA3, 0x00, 0xFF, 0x00, 0xFF, 0x00,
  0x00, 0xFF, 0x00, 0x00, 0x00, 0x4B, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0xFF, 0x33, 0x2F, 0x00,
  0x00, 0x00, 0xFF, 0x00, 0x00, 0x1F, 0x97, 0x00, 0xDF, 0x00, 0xFF, 0x00, 0x73, 0x00, 0x77, 0x00,
  0x6B, 0x7B, 0xC3, 0x00, 0x57, 0x57, 0xAB, 0x00, 0x57, 0x47, 0x93, 0x00, 0x53, 0x37, 0x7F, 0x00,
  0x4F, 0x27, 0x67, 0x00, 0x47, 0x1B, 0x4F, 0x00, 0x3B, 0x13, 0x3B, 0x00, 0x27, 0x77, 0x77, 0x00,
  0x23, 0x73, 0x73, 0x00, 0x1F, 0x6F, 0x6F, 0x00, 0x1B, 0x6B, 0x6B, 0x00, 0x1B, 0x67, 0x67, 0x00,
  0x1B, 0x6B, 0x6B, 0x00, 0x1F, 0x6F, 0x6F, 0x00, 0x23, 0x73, 0x73, 0x00, 0x27, 0x77, 0x77, 0x00,
  0xFF, 0xFF, 0xEF, 0x00, 0xF7, 0xF7, 0xDB, 0x00, 0xF3, 0xEF, 0xCB, 0x00, 0xEF, 0xEB, 0xBB, 0x00,
  0xF3, 0xEF, 0xCB, 0x00, 0xE7, 0x93, 0x07, 0x00, 0xE7, 0x97, 0x0F, 0x00, 0xEB, 0x9F, 0x17, 0x00,
  0xEF, 0xA3, 0x23, 0x00, 0xF3, 0xAB, 0x2B, 0x00, 0xF7, 0xB3, 0x37, 0x00, 0xEF, 0xA7, 0x27, 0x00,
  0xEB, 0x9F, 0x1B, 0x00, 0xE7, 0x97, 0x0F, 0x00, 0x0B, 0xCB, 0xFB, 0x00, 0x0B, 0xA3, 0xFB, 0x00,
  0x0B, 0x73, 0xFB, 0x00, 0x0B, 0x4B, 0xFB, 0x00, 0x0B, 0x23, 0xFB, 0x00, 0x0B, 0x73, 0xFB, 0x00,
  0x00, 0x13, 0x93, 0x00, 0x00, 0x0B, 0xD3, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0x00,
};

// forma de obtener los canales.
#define r(i) palette[i * 4 + 2]
#define g(i) palette[i * 4 + 1]
#define b(i) palette[i * 4 + 0]

/*

TILE TYPES:
  bit0 = game object
  bit1 = non-colliding, draws behind the player
  bit2 = colliding, middle layer
  bit3 = push/pull block
  bit4 = non-colliding, draws above the player (for tall objects that the player can walk behind)
  bit5 = mini map tile
  bit6 = weapon
  bit7 = item
  bit8 = character

FLAGS FOR WEAPONS:
  bit16 = light blaster
  bit17 = heavy blaster OR thermal detonator (????)
  bit18 = lightsaber
  bit19 = the force

FLAGS FOR ITEMS:
  bit16 = keycard
  bit17 = puzzle item
  bit18 = puzzle item (rare???)
  bit19 = puzzle item (key item???)
  bit20 = locator
  bit22 = health pack

FLAGS FOR CHARACTERS:
  bit16 = player
  bit17 = enemy
  bit18 = friendly

FLAGS FOR OTHER TILES:
  bit16 = door

FLAGS FOR MINI-MAP TILES:
  bit17 = specific mini map tile (home)
  bit18 = specific mini map tile (puzzle, unsolved)
  bit19 = specific mini map tile (puzzle solved)
  bit20 = specific mini map tile (gateway, unsolved)
  bit21 = specific mini map tile (gateway, solved)
  bit22 = specific mini map tile (up wall, locked)
  bit23 = specific mini map tile (down wall, locked)
  bit24 = specific mini map tile (left wall, locked)
  bit25 = specific mini map tile (right wall, locked)
  bit26 = specific mini map tile (up wall, unlocked)
  bit27 = specific mini map tile (down wall, unlocked)
  bit28 = specific mini map tile (left wall, unlocked)
  bit29 = specific mini map tile (right wall, unlocked)
  bit30 = specific mini map tile (objective)

*/

int write_ppm(const char* filename, unsigned int width, unsigned int height, char* data) {

  unsigned char* image_data = malloc(width * height * 3);
  unsigned int i;
  for (i = 0; i < width*height; i++) {
    unsigned int j = (i * 3);
    image_data[j + 0] = r(data[i]);
    image_data[j + 1] = g(data[i]);
    image_data[j + 2] = b(data[i]);
  }

  FILE* handle = fopen(filename, "wb");
  if (handle == NULL) {
    printf("No se puede escribir %s\n", filename);
    return 1;
  }

  fprintf(handle, "P6\n%d %d\n255\n", width, height);
  fwrite(image_data, width * height * 3, 1, handle);

  fclose(handle);

  free((void*)image_data);
  return 0;
}

int write_map(const char* filename, unsigned short width, unsigned short height, unsigned short* data) {
  FILE* handle = fopen(filename, "wb");
  if (handle == NULL) {
    printf("No se puede escribir %s\n", filename);
    return 1;
  }
  fwrite("TMAP", 4, 1, handle);
  fwrite(&width, 2, 1, handle);
  fwrite(&height, 2, 1, handle);
  fwrite(data, width * height * 2, 1, handle);
  fclose(handle);
  return 0;
}

/**
 * printf_tag
 *
 *
 */
void printf_tag(char *tag) {
  printf("%c", tag[0]);
  printf("%c", tag[1]);
  printf("%c", tag[2]);
  printf("%c\n", tag[3]);
}

/**
 * main
 *
 *
 */
int main(int argc, char** argv) {

  if (access(argv[1], R_OK) != 0) {
    printf("No existe el archivo %s\n", argv[1]);
    return EXIT_FAILURE;
  }

  // Abrimos YODESK.dta
  FILE* handle = fopen(argv[1],"rb");
  if (handle == NULL) {
    printf("No se pudo abrir el archivo %s\n", argv[1]);
    return EXIT_FAILURE;
  }

  unsigned int byte_offset;

  /**********************************************************************************
   *
   * Leemos la parte VERS del archivo.
   *
   **********************************************************************************/
  char vers[4];
  unsigned int vers_version = 0;

  fread(vers, 4, 1, handle);
  fread(&vers_version, 4, 1, handle);

  printf_tag(vers);
  printf("VERS.version: %d\n", vers_version);

  /**********************************************************************************
   *
   * Leemos la parte STUP del archivo.
   *
   **********************************************************************************/
  char stup[4];
  unsigned int stup_size = 0;
  char stup_image[288*288];

  fread(stup, 4, 1, handle);
  fread(&stup_size, 4, 1, handle);

  printf_tag(stup);
  printf("STUP.size: %d\n", stup_size);

  // leemos la imagen de configuración.
  fread(stup_image, 288*288, 1, handle);

  write_ppm("assets/screen.ppm", 288, 288, stup_image);

  /**********************************************************************************
   *
   * Leemos la parte SNDS del archivo.
   *
   **********************************************************************************/
  char snds[4];
  unsigned int snds_size = 0;
  unsigned char snds_count = 0;
  unsigned char snds_unknown = 0;

  char snd_name[256];
  unsigned short snd_length = 0;

  fread(snds, 4, 1, handle);
  fread(&snds_size, 4, 1, handle);
  fread(&snds_count, 1, 1, handle);
  fread(&snds_unknown, 1, 1, handle);

  printf_tag(snds);
  printf("SNDS.size: %d\n", snds_size);
  printf("SNDS.count: %d\n", snds_count);
  printf("SNDS.unknown: %d\n", snds_unknown);

  byte_offset = 6;
  while (byte_offset < snds_size) {
    fread(&snd_length, 2, 1, handle);
    fread(snd_name, snd_length, 1, handle);

    printf("%s\n", snd_name);

    byte_offset += (2 + snd_length);
  }

  /**********************************************************************************
   *
   * Leemos la parte TILE del archivo.
   *
   **********************************************************************************/
  char tile[4];
  unsigned int tile_size = 0;
  unsigned int tile_count = 0;
  unsigned int tile_index = 0;
  unsigned int tile_flags = 0;

  char tile_image[1024];
  //unsigned int tile_id;

  fread(tile, 4, 1, handle);
  fread(&tile_size, 4, 1, handle);

  printf_tag(tile);
  printf("TILE.size: %d\n", tile_size);

  tile_count = (tile_size / 1028);

  printf("TILE.count: %d\n", tile_count);
  for (tile_index = 0; tile_index < tile_count; tile_index++) {
    char tile_filename[1024];
    sprintf(tile_filename, "assets/tiles/%04d.ppm", tile_index);

    fread(&tile_flags, 4, 1, handle);
    fread(tile_image, 1024, 1, handle);

    write_ppm(tile_filename, 32, 32, tile_image);
    // TODO: Aquí habría que guardar el tile.
    //printf("%04d %08X\n", tile_index, tile_flags);
  }

  /**********************************************************************************
   *
   * Leemos la parte ZONE del archivo.
   *
   **********************************************************************************/
  char zone[4];
  unsigned int zone_size = 0;
  unsigned short zone_count;
  //unsigned short zone_unknown;
  unsigned int zone_index;

  fread(zone, 4, 1, handle);
  fread(&zone_count, 2, 1, handle);

  /*4 bytes: "IZON"
  4 bytes: unknown
  2 bytes: map width (W)
  2 bytes: map height (H)
  1 byte: map flags (unknown meanings)
  5 bytes: unused (same values for every map)
  1 byte: planet (0x01 = desert, 0x02 = snow, 0x03 = forest, 0x05 = swamp)
  1 byte: unused (same values for every map)
  W*H*6 bytes: map data
  2 bytes: object info entry count (X)
  X*12 bytes: object info data
  4 bytes: "IZAX"
  2 bytes: length (X)
  X-6 bytes: IZAX data
  4 bytes: "IZX2"
  2 bytes: length (X)
  X-6 bytes: IZX2data
  4 bytes: "IZX3"
  2 bytes: length (X)
  X-6 bytes: IZX3 data
  4 bytes: "IZX4"
  8 bytes: IZX4 data
  4 bytes: "IACT"
  4 bytes: length (X)
  X bytes: action data
  4 bytes: "IACT"
  4 bytes: length (X)
  X bytes: action data
  ...
  4 bytes: "IACT"
  4 bytes: length (X)
  X bytes: action data
  */

  printf_tag(zone);
  printf("ZONE.size: %d\n", zone_size);
  printf("ZONE.count: %d\n", zone_count);

  unsigned short izon_unk0;
  unsigned short izon_unk1;
  unsigned short izon_unk2;
  unsigned short izon_id;

  FILE* zone_handle = fopen("assets/zones.json", "w");
  if (zone_handle == NULL) {
    fclose(handle);
    printf("No se puede escribir assets/zones.json");
    return EXIT_FAILURE;
  }

  //unsigned int iact_name_values[0xFFFFFF];
  //memset((void*)iact_name_values, 0, 0xFFFFFF);

  unsigned short iact_type_values[0xFFFF];
  memset((void*)iact_type_values, 0, 0xFFFF);

  unsigned short iact_subtype_values[0xFFFF];
  memset((void*)iact_subtype_values, 0, 0xFFFF);

  unsigned short iact_x_values[0xFFFF];
  memset((void*)iact_x_values, 0, 0xFFFF);

  unsigned short iact_y_values[0xFFFF];
  memset((void*)iact_y_values, 0, 0xFFFF);

  unsigned short iact_z_values[0xFFFF];
  memset((void*)iact_z_values, 0, 0xFFFF);

  fprintf(zone_handle, "[\n");
  for (zone_index = 0; zone_index < zone_count; zone_index++) {

    char izon[4];
    unsigned int izon_size;
    unsigned short izon_width;
    unsigned short izon_height;
    unsigned short izon_x;
    unsigned short izon_y;
    unsigned char izon_flags;
    unsigned char izon_type;
    unsigned short izon_layer[3];
    unsigned short izon_object_count;
    unsigned short izon_object_index;
    unsigned short izon_object_type;
    unsigned short izon_object_x;
    unsigned short izon_object_y;
    unsigned short izon_object_arg;

    fread(&izon_unk0, 2, 1, handle);
    fread(&izon_unk1, 2, 1, handle);
    fread(&izon_unk2, 2, 1, handle);
    fread(&izon_id, 2, 1, handle); // ID

    printf("%04X %04X %04X %04X\n", izon_unk0, izon_unk1, izon_unk2, izon_id);

    fread(izon, 4, 1, handle);
    fread(&izon_size, 4, 1, handle);
    fread(&izon_width, 2, 1, handle);
    fread(&izon_height, 2, 1, handle);
    fread(&izon_flags, 1, 1, handle);
    fseek(handle, 5, SEEK_CUR);
    fread(&izon_type, 1, 1, handle);

    printf_tag(izon);
    printf("ZONE.index: %d\n", zone_index);
    printf("IZON.width: %d\n", izon_width);
    printf("IZON.height: %d\n", izon_height);

    switch (izon_type) {
      default:
      case ZONE_TYPE_DESERT: printf("IZON.type: Desert\n"); break;
      case ZONE_TYPE_SNOW: printf("IZON.type: Snow\n"); break;
      case ZONE_TYPE_FOREST: printf("IZON.type: Forest\n"); break;
      case ZONE_TYPE_SWAMP: printf("IZON.type: Swamp\n"); break;
    }

    //unsigned int zone_width = izon_width * 32;
    //unsigned int zone_height = izon_height * 32;

    fseek(handle, 1, SEEK_CUR);
    fprintf(zone_handle, "  {\n");
    fprintf(zone_handle, "    \"id\": %d,\n", izon_id);
    fprintf(zone_handle, "    \"type\": %d,\n", izon_type);
    fprintf(zone_handle, "    \"width\": %d,\n", izon_width);
    fprintf(zone_handle, "    \"height\": %d,\n", izon_height);
    fprintf(zone_handle, "    \"tiles\": [\n");
    for (izon_x = 0; izon_x < izon_width; izon_x++) {
      fprintf(zone_handle, "      [");
      for (izon_y = 0; izon_y < izon_height; izon_y++) {
        fread(&izon_layer[0], 2, 1, handle);
        fread(&izon_layer[1], 2, 1, handle);
        fread(&izon_layer[2], 2, 1, handle);

        unsigned int layer_index = 0;

        fprintf(zone_handle, "[");
        for (layer_index = 0; layer_index < 3; layer_index++) {
          if (izon_layer[layer_index] == 0xFFFF) {
            fprintf(zone_handle, "null");
          } else {
            fprintf(zone_handle, "%d", izon_layer[layer_index]);
          }
          if (layer_index < 2) {
            fprintf(zone_handle, ",");
          }
        }
        fprintf(zone_handle, "]");
        if (izon_y < izon_height - 1) {
          fprintf(zone_handle, ",");
        }
      }
      fprintf(zone_handle, "]");
      if (izon_x < izon_width - 1) {
        fprintf(zone_handle, ",\n");
      } else {
        fprintf(zone_handle, "\n");
      }
    }
    fprintf(zone_handle, "    ]\n");
    fprintf(zone_handle, "  }");
    if (zone_index < zone_count - 1) {
      fprintf(zone_handle, ",\n");
    } else {
      fprintf(zone_handle, "\n");
    }

    fread(&izon_object_count, 2, 1, handle);
    for (izon_object_index = 0; izon_object_index < izon_object_count; izon_object_index++) {

      fread(&izon_object_type, 2, 1, handle);
      fseek(handle, 2, SEEK_CUR);

      fread(&izon_object_x, 2, 1, handle);
      fread(&izon_object_y, 2, 1, handle);
      fseek(handle, 2, SEEK_CUR);

      fread(&izon_object_arg, 2, 1, handle);

    }

    char izax[4];
    unsigned short izax_size;

    fread(izax, 4, 1, handle);
    fread(&izax_size, 2, 1, handle);
    fseek(handle, izax_size - 6, SEEK_CUR);

    printf_tag(izax);

    char izx2[4];
    unsigned short izx2_size;

    fread(izx2, 4, 1, handle);
    fread(&izx2_size, 2, 1, handle);
    fseek(handle, izx2_size - 6, SEEK_CUR);

    printf_tag(izx2);

    char izx3[4];
    unsigned short izx3_size;

    fread(izx3, 4, 1, handle);
    fread(&izx3_size, 2, 1, handle);
    fseek(handle, izx3_size - 6, SEEK_CUR);

    printf_tag(izx3);

    char izx4[4];
    //unsigned short izx4_size;

    fread(izx4, 4, 1, handle);
    fseek(handle, 6, SEEK_CUR);

    printf_tag(izx4);

    char iact[4];
    unsigned int iact_size;
    unsigned short iact_count;
    unsigned short iact_index;
    unsigned short iact_type;
    unsigned short iact_subtype;
    unsigned short iact_x;
    unsigned short iact_y;
    unsigned short iact_z;
    //unsigned short iact_w;

    unsigned int pos;

    //unsigned int iact_name_value;

    char iact_name[8];
    memset((void*)iact_name, 0, 8);

    unsigned short iact_text_length;
    char iact_text[0xFFFF];
    memset((void*)iact_text, 0, 0xFFFF);

    fread(&iact_count, 2, 1, handle);
    printf("IACT.count: %d\n", iact_count);
    for (iact_index = 0; iact_index < iact_count; iact_index++) {

      printf("\n");

      printf("IACT.index: %d\n", iact_index);

      fread(iact, 4, 1, handle);
      fread(&iact_size, 4, 1, handle);

      pos = ftell(handle);

      fread(&iact_type, 2, 1, handle);
      iact_type_values[iact_type]++;
      //fseek(handle, iact_size - 2, SEEK_CUR);

      printf_tag(iact);
      printf("IACT.size: %d\n", iact_size);
      printf("IACT.type: %04X %d\n", iact_type, iact_type);

      printf("\n");

      byte_offset = 0;
      while (byte_offset < (iact_size - 2)) {

        fread(&iact_subtype, 2, 1, handle);
        iact_subtype_values[iact_subtype]++;

        fread(&iact_x, 2, 1, handle);
        iact_x_values[iact_x]++;

        fread(&iact_y, 2, 1, handle);
        iact_y_values[iact_y]++;

        fread(&iact_z, 2, 1, handle);
        iact_z_values[iact_z]++;

        fread(iact_name, 6, 1, handle);

        //fseek(handle, -6, SEEK_CUR);
        //fread(&iact_name_value, 6, 1, handle);
        //iact_name_values[iact_name_value]++;

        byte_offset += 2 + 2 + 2 + 2 + 6;

        printf("\n");

        printf("IACT.subtype: %04X %d\n", iact_subtype, iact_subtype);
        printf("IACT.x: %04X %d\n", iact_x, iact_x);
        printf("IACT.y: %04X %d\n", iact_y, iact_y);
        printf("IACT.z: %04X %d\n", iact_z, iact_z);

        printf("IACT.name: %s\n", iact_name);
        if (strcmp("ShowTe", iact_name) == 0) {

          fread(&iact_text_length, 2, 1, handle);

          memset((void*)iact_text, 0, 0xFFFF);
          fread(iact_text, iact_text_length, 1, handle);

          byte_offset += 2 + iact_text_length;

          printf("IACT.text: %s\n", iact_text);

        }

        printf("\n");

      }

      fseek(handle, pos, SEEK_SET);
      fseek(handle, iact_size, SEEK_CUR);

      // Las acciones tienen una longitud de 0xE

      // ¿Cómo creo que funciona el tema de los IACT?
      // Los IACT poseen en algún sitio un identificador
      // que indica qué tipo de trigger lanza esa acción.
      // Donde los posibles triggers son:

      // - Matar a un enemigo
      // - Colocarte en una casilla en concreto (post-enter)
      // - Intentar entrar en una casilla en concreto (pre-enter)
      // - Coger un item
      // - Abrir una puerta
      // - Entrar en una zona
      // - Salir de una zona

      // Por lo tanto el tipo de IACT indicará si el IACT posee
      // coordenadas o no. Es posible que las coordenadas
      // estén en todos los IACT a pesar de que no se usen.

      // En los IACT creo que he descubierto que desde el código
      // legible hacia atrás hay 6 bytes donde el primero o los
      // dos primeros indican el tipo de instrucción.

      // 0600 Redraw
      // D000 Ending
      // 1600 HotSpo
      // 1500 HotSpo

      // LONGITUDES DE LOS IACT
      // Longitud por tipo
      // HEX  HEX  DEC
      // TIPO SUBT LONG
      // 0001 0001 1040
      // 0001 0001 0032
      // 0001 0002 0102
      // 0001 0006 0550
      // 0001 0009 0032
      // 0002 0000 0046
      // 0002 0002 0099
      // 0002 0003 0046
      // 0002 0006 0120
      // 0003 0001 0060
      // 0003 0002 0090
      // 0003 0002 0088
      // 0003 0002 0194
      // 0003 0002 0184

      // INSTRUCCIONES
      // Play
      // se\388
      // ImpOff
      // Boushh
      // Enemy0
      // Redraw
      // Repl
      // Requ
      // Move
      // move
      // Coun
      // Bump
      // HotSpo
      // Remove
      // ShowTe
      // Show
      // PlaySo
      // WaitFo
      // SetCou
      // ThrowR 00 <?
      // <No
      // MS San
      // Dn
      // SetHer
      // SetRan 00 <- SetRandomSeed
      // Zone T
      // Rand
      // opIt
      // HotS
      // Glob
      // ShowFr
      // TileAt
      // Enem
      // Redr
      // DropIt
      // AddToH
      // Hero
      // HasIte
      // GoItem
      // GOIt
      // GOItem
      // EveryE
      // ZX:0
      // ZX:01
      // ZX:02
      // ZX:03
      // ZX:04

      //6 bytes se\388
      //2 bytes longitud de la cadena de texto
      //X bytes texto

    }

  }

  fprintf(zone_handle, "]\n");
  fclose(zone_handle);

  unsigned int iact_value_index = 0;

  printf("IACT.type values\n");
  for (iact_value_index = 0; iact_value_index < 0xFFFF; iact_value_index++) {
    if (iact_type_values[iact_value_index] > 0) {
      printf("%04X: %04X %d\n", iact_value_index, iact_type_values[iact_value_index], iact_type_values[iact_value_index]);
    }
  }

  printf("IACT.subtype values\n");
  for (iact_value_index = 0; iact_value_index < 0xFFFF; iact_value_index++) {
    if (iact_subtype_values[iact_value_index] > 0) {
      printf("%04X: %04X %d\n", iact_value_index, iact_subtype_values[iact_value_index], iact_subtype_values[iact_value_index]);
    }
  }

  printf("IACT.x values\n");
  for (iact_value_index = 0; iact_value_index < 0xFFFF; iact_value_index++) {
    if (iact_x_values[iact_value_index] > 0) {
      printf("%04X: %04X %d\n", iact_value_index, iact_x_values[iact_value_index], iact_x_values[iact_value_index]);
    }
  }

  printf("IACT.y values\n");
  for (iact_value_index = 0; iact_value_index < 0xFFFF; iact_value_index++) {
    if (iact_y_values[iact_value_index] > 0) {
      printf("%04X: %04X %d\n", iact_value_index, iact_y_values[iact_value_index], iact_y_values[iact_value_index]);
    }
  }

  printf("IACT.z values\n");
  for (iact_value_index = 0; iact_value_index < 0xFFFF; iact_value_index++) {
    if (iact_z_values[iact_value_index] > 0) {
      printf("%04X: %04X %d\n", iact_value_index, iact_z_values[iact_value_index], iact_z_values[iact_value_index]);
    }
  }

  /*
  printf("IACT.name values\n");
  for (iact_value_index = 0; iact_value_index < 0xFFFFFF; iact_value_index++) {
    if (iact_name_values[iact_value_index] > 0) {
      printf("%08X: %08X\n", iact_value_index, iact_name_values[iact_value_index], iact_name_values[iact_value_index]);
    }
  }
  */

  /**********************************************************************************
   *
   * Leemos PUZ2.
   *
   **********************************************************************************/
  char puz2[4];
  fread(puz2, 4, 1, handle);
  printf_tag(puz2);

  unsigned int puz2_size;
  fread(&puz2_size, 4, 1, handle);
  printf("%d %08X\n", puz2_size, puz2_size);

  // después del IPUZ hay 20 bytes que sirven para cosas
  // y después de esos 20 bytes hay dos que valen para indicar
  // la longitud de los textos, cada texto termina en 00
  // y después otra cadena. Los puzzles lo único que contienen
  // además de estos 20 bytes, es los textos.
  unsigned short ipuz_id;
  char ipuz[4];
  unsigned int ipuz_size;
  unsigned int ipuz_type;
  unsigned int ipuz_unk1;
  unsigned int ipuz_unk2;
  unsigned short ipuz_unk3;
  unsigned short ipuz_unk4;
  unsigned short ipuz_item1;
  unsigned short ipuz_item2;
  unsigned short ipuz_item3;
  unsigned short ipuz_item4;

  unsigned short ipuz_take_length;
  unsigned short ipuz_thx_length;
  unsigned short ipuz_give_length;

  char ipuz_take[0xFFFF];
  char ipuz_thx[0xFFFF];
  char ipuz_give[0xFFFF];

  byte_offset = 0;
  while (byte_offset < puz2_size) {
    fread(&ipuz_id, 2, 1, handle);
    if (ipuz_id == 0xFFFF) {
      break;
    }

    fread(ipuz, 4, 1, handle);
    fread(&ipuz_size, 4, 1, handle);

    // WTF
    fread(&ipuz_type, 4, 1, handle);
    fread(&ipuz_unk1, 4, 1, handle);
    fread(&ipuz_unk2, 4, 1, handle); // 0x00000000, 0x00000004, 0xFFFFFFFF
    fread(&ipuz_unk3, 2, 1, handle); // 0x0137, 0x013F, 0x011F, 0x011E, 0x0080, 0x0000
    if (ipuz_type == 3) {
      fread(&ipuz_unk4, 2, 1, handle);
    }

    memset((void*)ipuz_take, 0, 0xFFFF);
    memset((void*)ipuz_thx, 0, 0xFFFF);
    memset((void*)ipuz_give, 0, 0xFFFF);

    fread(&ipuz_take_length, 2, 1, handle);
    if (ipuz_take_length > 0) {
      fread(ipuz_take, ipuz_take_length, 1, handle);
    }

    fread(&ipuz_thx_length, 2, 1, handle);
    if (ipuz_thx_length > 0) {
      fread(ipuz_thx, ipuz_thx_length, 1, handle);
    }

    fread(&ipuz_give_length, 2, 1, handle);
    if (ipuz_give_length > 0) {
      fread(ipuz_give, ipuz_give_length, 1, handle);
    }

    // TODO: Aquí habría que leer el texto del puzzle.

    // al final de todo el puzzle hay
    // 8 bytes que ni puta idea de lo que son.
    fread(&ipuz_item1, 2, 1, handle);
    fread(&ipuz_item2, 2, 1, handle);

    // ¡IMPORTANTE! DE LOS ÚLTIMOS 8 BYTES, EL 4º (SHORT) ES
    // EL ITEM QUE NECESITAS PARA COMPLETAR EL PUZZLE.
    fread(&ipuz_item3, 2, 1, handle); // ITEM.

    if (ipuz_type != 3) {
      fread(&ipuz_item4, 2, 1, handle);
    }

    //fseek(handle, ipuz_size - ipuz_take_length - ipuz_give_length - ipuz_thx_length - 14, SEEK_CUR);

    printf_tag(ipuz);
    printf("IPUZ.id: %d\n", ipuz_id);
    printf("IPUZ.type: %08X %d\n", ipuz_type, ipuz_type);
    printf("IPUZ.unk1: %08X %d\n", ipuz_unk1, ipuz_unk1);
    printf("IPUZ.unk2: %08X %d\n", ipuz_unk2, ipuz_unk2);
    printf("IPUZ.unk3: %04X %d\n", ipuz_unk3, ipuz_unk3);
    if (ipuz_type == 3) {
      printf("IPUZ.unk4: %04X %d\n", ipuz_unk4, ipuz_unk4);
    }

    printf("IPUZ.take: %d %s\n", ipuz_take_length, ipuz_take);
    printf("IPUZ.thx: %d %s\n", ipuz_thx_length, ipuz_thx);
    printf("IPUZ.give: %d %s\n", ipuz_give_length, ipuz_give);

    printf("IPUZ.item1: %04X %d\n", ipuz_item1, ipuz_item1);
    printf("IPUZ.item2: %04X %d\n", ipuz_item2, ipuz_item2);
    printf("IPUZ.item3: %04X %d\n", ipuz_item3, ipuz_item3);
    if (ipuz_type != 3) {
      printf("IPUZ.item4: %04X %d\n", ipuz_item4, ipuz_item4);
    }

    // Acabo de entender el tema de los puzzles, los puzzles
    // tienen 3 partes, NECESITO, GRACIAS y TOMA.

    byte_offset += (ipuz_size + 10);
  }

  /**********************************************************************************
   *
   * Leemos CHAR
   *
   **********************************************************************************/
  char chra[4]; // CHAR
  unsigned int chra_size;

  fread(chra, 4, 1, handle);
  fread(&chra_size, 4, 1, handle);

  printf_tag(chra);
  printf("%d %08X\n", chra_size, chra_size);

  unsigned short icha_id;
  char icha[4];
  char icha_name[16];
  unsigned short icha_properties[ICHA_NUM_PROPERTIES];
  unsigned int icha_size;

  byte_offset = 0;
  while (byte_offset < chra_size) {
    fread(&icha_id, 2, 1, handle);
    if (icha_id == 0xFFFF) {
      break;
    }

    fread(icha, 4, 1, handle);
    fread(&icha_size, 4, 1, handle);
    fread(icha_name, 16, 1, handle);
    fread(icha_properties, ICHA_NUM_PROPERTIES * 2, 1, handle);
    if (ICHA_NUM_PROPERTIES < 29) {
      fseek(handle, icha_size - (16 * (ICHA_NUM_PROPERTIES * 2)), SEEK_CUR);
    }

    printf_tag(icha);
    printf("ICHA.id: %d\n", icha_id);
    printf("ICHA.name: %s\n", icha_name);

    unsigned char k;
    for (k = 0; k < ICHA_NUM_PROPERTIES; k++) {
      printf("ICHA.property: %04X %04d\n", icha_properties[k], icha_properties[k]);
    }

    byte_offset += (icha_size + 10);
  }

  // después de la longitud lo que tenemos son dos bytes
  // que identifican un ICHA, después 4 bytes, después
  // el nombre zstr.

  /**********************************************************************************
   *
   * Leemos CHWP
   *
   **********************************************************************************/
  char chwp[4]; // CHWP
  fread(chwp, 4, 1, handle);
  printf_tag(chwp);

  // esto parece ser una tabla que relaciona personajes
  // con armas

  unsigned int chwp_size;
  unsigned short chwp_id;
  unsigned short chwp_unk0;
  unsigned short chwp_unk1;
  fread(&chwp_size, 4, 1, handle);
  printf("%d %08X\n", chwp_size, chwp_size);

  byte_offset = 0;
  while (byte_offset < chwp_size) {
    fread(&chwp_id, 2, 1, handle);
    if (chwp_id == 0xFFFF) {
      break;
    }
    fread(&chwp_unk0, 2, 1, handle);
    fread(&chwp_unk1, 2, 1, handle);

    printf("CHWP.id: %d\n", chwp_id);
    printf("CHWP.un0: %d\n", chwp_unk0);
    printf("CHWP.un1: %d\n", chwp_unk1);

    byte_offset += 6;
  }

  /**********************************************************************************
   *
   * Leemos la parte CAUX
   *
   **********************************************************************************/
  char caux[4]; // CAUX
  fread(caux, 4, 1, handle);
  printf_tag(caux);

  // NI PUTA IDEA

  unsigned int caux_size;
  unsigned short caux_id;
  unsigned short caux_unk;
  fread(&caux_size, 4, 1, handle);
  printf("%d %08X\n", caux_size, caux_size);

  byte_offset = 0;
  while (byte_offset < caux_size) {
    fread(&caux_id, 2, 1, handle);
    if (caux_id == 0xFFFF) {
      break;
    }
    fread(&caux_unk, 2, 1, handle);

    printf("CAUX.id: %d\n", caux_id);
    printf("CAUX.unk: %d\n", caux_unk);

    byte_offset += 4;
  }

  /**********************************************************************************
   *
   * Leemos la parte TNAM del archivo.
   *
   **********************************************************************************/
  char tnam[4]; // TNAM
  fread(tnam, 4, 1, handle);
  printf_tag(tnam);

  // Nombres de los items.

  unsigned int tnam_size;
  fread(&tnam_size, 4, 1, handle);
  printf("%d %08X\n", tnam_size, tnam_size);

  char name[32]; // nombre.
  unsigned short name_id; // identificador del nombre.

  // 0x18
  // 0x18
  byte_offset = 0;
  while (byte_offset < tnam_size) {
    fread(&name_id, 2, 1, handle);
    if (name_id == 0xFFFF) {
      break;
    }
    fread(name, 0x18, 1, handle);

    printf("NAME: %d %s\n", name_id, name);

    byte_offset += 2 + 0x18;
  }

  /**********************************************************************************
   *
   * Final del archivo.
   *
   **********************************************************************************/
  char endf[4];
  fread(endf, 4, 1, handle);
  printf_tag(endf);

  fclose(handle);

  return EXIT_SUCCESS;

}
